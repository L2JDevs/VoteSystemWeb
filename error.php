<?php
// Initialize the session
session_start();

// Include language file
include_once 'common.php';
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	header("location: index.php");
	exit;
}

header( "refresh:5; url=logout.php" ); 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
	<script type="text/javascript" src="js/conditions.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; }
	</style>
	
</head>
<body>
	<div class="page-header">
		<h1><b><?php echo $lang['reward_no_char_title'];?></b></h1>
		<h2><?php echo $lang['reward_no_char_text'];?></h2>
		<br>
	</div>
</body>
</html>