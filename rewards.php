<?php
// Initialize the session
session_start();

// Include language file
include_once 'common.php';
include_once 'system/CharacterValues.php';
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	header("location: index.php");
	exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
	<script type="text/javascript" src="js/conditions.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; }
	</style>
</head>
<body>
	<div class="page-header">
		<h1><b><?php echo $lang['reward_title_text'];?></b></h1>
		<h3><?php echo $lang['reward_info_text'];?></h3>
		<b>*</b> <?php echo $lang['reward_warning'];?><br>
		<br>
		<?php
			foreach ($charlist as $valor)
			{
				$id = ($valor ['id']);
				$name = ($valor ['name']);
		?>
				<a id="boton" onclick="sendReward(this, <?php echo $id;?>)" class="btn btn-green"><?php echo $name;?></a>
		<?php 
			}
		?>
		<br>
	</div>
	<p>
		 <a href="logout.php" class="btn btn-danger"><?php echo $lang['logout'];?></a> 
	</p>
</body>
</html>