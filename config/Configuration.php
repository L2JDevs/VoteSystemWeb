<?php

// -----------------------------------------------
// Configuration values:
// -----------------------------------------------

// Web address where to create an account:
// Default = "http://localhost/account";
$register_link = "http://localhost/account";

// Waiting time to next vote (seconds):
// Default = 10;
$time_delay = 10;

// Waiting time to vote again (hours):
// Default = 24;
$time_reload = 24;

// Specify where to send the reward:
// INVENTORY = Send the reward to the inventory of the indicated player.
// WAREHOUSE = Send the reward to the warehouse of the indicated player.
// Default = "INVENTORY";
$reward_type = "INVENTORY";

// List of rewards to send to the player:
// Default = ['57' => '250000', '6393' => '1'];
$reward_list = ['57' => '250000', '6393' => '1'];

?>