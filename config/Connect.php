<?php

// Login server host values:
$ls_hostname = 'localhost';
$ls_username = 'root';
$ls_password ='password';
$ls_database = 'l2jdevs_loginserver';

// Game server host values:
$gs_hostname = 'localhost';
$gs_username = 'root';
$gs_password ='password';
$gs_database = 'l2jdevs_gameserver';

// Attempt to connect to MySQLi database:
$link1 = mysqli_connect($ls_hostname, $ls_username, $ls_password, $ls_database);
$link2 = mysqli_connect($gs_hostname, $gs_username, $gs_password, $gs_database);

// Check connection
if(!$link1)
{
    die("ERROR: Could not connect to login server. " .$link1->connect_error);
}
if(!$link2)
{
    die("ERROR: Could not connect to game server. " .$link2->connect_error);;
}

?>