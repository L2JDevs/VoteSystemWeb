<?php

session_start();

include_once 'common.php';
 
require_once "config/Connect.php";
require_once "config/Configuration.php";
include_once "system/NetworkValues.php";
 
if (isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true)
{
	header("location: index.php");
	exit;
}
 
$username = $password = $charname = "";
$username_err = $password_err = "";
 
if ($_SERVER["REQUEST_METHOD"] == "POST")
{
	if (empty(trim($_POST["username"])))
	{
		$username_err = $lang['login_name_empty'];
	}
	else
	{
		$username = trim($_POST["username"]);
	}
	
	if (empty(trim($_POST["password"])))
	{
		$password_err = $lang['login_password_empty'];
	}
	else
	{
		$password = trim($_POST["password"]);
	}
	
	if (empty($username_err) && empty($password_err))
	{
		$sql = "SELECT login, password FROM accounts WHERE login = ?";
		if ($stmt = mysqli_prepare($link1, $sql))
		{
			mysqli_stmt_bind_param($stmt, "s", $param_username);
			$param_username = $username;
			
			if (mysqli_stmt_execute($stmt))
			{
				mysqli_stmt_store_result($stmt);
				if (mysqli_stmt_num_rows($stmt) == 1)
				{					
					mysqli_stmt_bind_result($stmt, $username, $hashed_password);
					if (mysqli_stmt_fetch($stmt))
					{
						if (strcmp(base64_encode(pack('H*', sha1($password))), $hashed_password) == 0)
						{
							$_SESSION["username"] = $username;
							$_SESSION["charname"] = $charname;
							
							$sql1 = "SELECT votedTime FROM vote_system_login WHERE login = '".$username."'";
							if ($stmt = mysqli_prepare($link2, $sql1))
							{
								if (mysqli_stmt_execute($stmt))
								{
									mysqli_stmt_store_result($stmt);
									
									function isDone()
									{
										session_start();
										
										$_SESSION["loggedin"] = true;
										header("location: votes.php");
									}
									
									if (mysqli_stmt_num_rows($stmt) == 1)
									{
										mysqli_stmt_bind_result($stmt, $userTime);
										if (mysqli_stmt_fetch($stmt))
										{
											$date = new DateTime();
											$newDate = $date->format('U');
											$myDate = DateTime::createFromFormat('d/m/Y H:i:s', $userTime);
											$myTimeStamp = $myDate->getTimestamp();
											
											$timediff = ($newDate - $myTimeStamp);
											if ($timediff > ($time_reload * 3600))
											{ 
												$sql2 = "DELETE FROM vote_system_login WHERE login = '".$username."'";
												if ($link2->query($sql2) == TRUE)
												{
													isDone();
												}
											}
											else
											{
												$username_err = $lang['reward_already_time'];
											}
										}
										else
										{
											if (!$isDoneIP)
											{
												$username_err = $lang['reward_already_ip'];
											}
											else
											{
												isDone();
											}
										}
									}
									else
									{
										if (!$isDoneIP)
										{
											$username_err = $lang['reward_already_ip'];
										}
										else
										{
											isDone();
										}
									}
								}
							}
						}
						else
						{
							$password_err = $lang['login_password_invalid'];
						}
					}
				}
				else
				{
					$username_err = $lang['login_name_error'];
				}
			}
			else
			{
				echo $lang['login_error'];
			}
			
			mysqli_stmt_close($stmt);
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; }
		.wrapper{ width: 350px; padding: 20px; }
	</style>
</head>
<body>
	<div class="wrapper">
		<h2><?php echo $lang['login'];?></h2>
		<p><?php echo $lang['login_info'];?></p>
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
			<div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
				<label><?php echo $lang['username'];?></label>
				<input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
				<span class="help-block"><?php echo $username_err; ?></span>
			</div>	
			<div class="form-group <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
				<label><?php echo $lang['password'];?></label>
				<input type="password" name="password" class="form-control">
				<span class="help-block"><?php echo $password_err; ?></span>
			</div>
			<div class="form-group">
				<input type="submit" class="btn btn-primary" value="<?php echo $lang['connect'];?>">
			</div>
			<p>
				<?php echo $lang['login_create_text_1'];?>
				<a target="_blank" href="<?php echo $register_link;?>"><?php echo $lang['login_create_text_2'];?></a>
			</p>
			<p>
				<font color="#000000" size="2"><?php echo $lang['vote_count'];?>
				</font> <font color="#337ab7" size="2">
					<?php 
						$votes = 0;
						$sqlvotes = "SELECT votes FROM vote_system_count";
						$result = mysqli_query($link2, $sqlvotes);
						if (mysqli_num_rows($result) > 0)
						{
							while($row = mysqli_fetch_assoc($result))
							{
								$votes = $row["votes"];
							}
						}
						echo $votes;
						
						mysqli_close($link1);
						mysqli_close($link2);
					?> 
				</font>
			</p>
			<p>
				<a href="?lang=en"><img src="img/flag/en.png"></a>
				<a href="?lang=es"><img src="img/flag/es.png"></a>
			</p>
		</form>
	</div>	
</body>
</html>