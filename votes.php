<?php
// Initialize the session
session_start();

// Include language file
include_once 'common.php';
include_once 'system/VoteValues.php';
require_once "config/Configuration.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	header("location: index.php");
	exit;
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
	<script type="text/javascript" src="js/conditions.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; }
	</style>
</head>
<body>
	<div class="page-header">
		<h1><b><?php echo $lang['vote_title_text'];?></b></h1>
		<h3><?php echo $lang['vote_info_text'];?></h3>
		<br>
		
		<?php 
				foreach ($xmlList->vote as $nodo)
				{
					$value = $nodo->attributes();
					
					$voteId = $value['id'];
					$voteName = $value['name'];
					$voteImage = $value['img'];
					$voteLink = $value['link'];
			?>
					<a id="boton" type="<?php echo $voteLink;?>" target="_blank" onclick="checkVoteId(this, <?php echo $voteId;?>, <?php echo $time_delay;?>)"><img src="<?php echo $voteImage;?>"></a>
			<?php 
				}
			?>
			
			<br>
			<br>
			<a id="boton" onclick="checkReward(this, <?php echo $xmlVoteCount;?>)" class="btn btn-green"><?php echo $lang['claim_rewards'];?></a>
		<br>
		<br>
		<b>*</b> <?php echo $lang['vote_warning'];?><br>
	</div>
	<p>
		 <a href="logout.php" class="btn btn-danger"><?php echo $lang['logout'];?></a>
		 
	</p>
</body>
</html>