<?php

$xmlFile = 'config/VoteList.xml';

if (file_exists($xmlFile))
{
	$xml = simplexml_load_file($xmlFile);
	$xmlList = $xml;
	$xmlVoteCount = count($xml->vote);
}
else
{
	exit('Error to load xml vote list file.');
}

?>