<?php

require_once "config/Connect.php";

$username = $_SESSION["username"];
$charlist = null;

$sql = "SELECT charId, char_name, online FROM characters WHERE account_name = ?";
if ($stmt = mysqli_prepare($link2, $sql))
{
	mysqli_stmt_bind_param($stmt, "s", $username);
	if (mysqli_stmt_execute($stmt))
	{
		mysqli_stmt_store_result($stmt);
		if (mysqli_stmt_num_rows($stmt) >= 1)
		{
			$stmt->bind_result($charid, $charname, $state);
			while ($stmt->fetch()) 
			{
				if ($state == 0)
				{
					$value = array("id" => $charid, "name" => $charname);
					$charlist[] = array_merge($value);
				}
			}
		}
		else
		{
			header("location: error.php");
		}
		
		if ($charlist == null)
		{
			header("location: error.php");
		}
	}
	else
	{
	    echo $lang['login_error'];
	}
}

mysqli_stmt_close($stmt);
mysqli_close($link2);
?>