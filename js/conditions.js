var voteDoneId=1;

function sendReward(node, id)
{
	if (node == null)
	{
		return;
	}
	
	if (id == null)
	{
		return;
	}
	
	return node.setAttribute('href', "done.php?key=" + id);
}

function checkVoteId(node, id, time)
{
	if (node == null)
	{
		return;
	}
	
	if (id == null)
	{
		return;
	}
	
	if (time == null)
	{
		return;
	}
	
	if (voteDoneId < id)
	{
		return;
	}
	
	if (voteDoneId > id)
	{
		return;
	}
	
	node.style.pointerEvents = 'none';
	node.style.color = 'red';
	
	var url = node.getAttribute("type");
	node.setAttribute('href', url);
	
	setTimeout(function()
	{
		voteDoneId++
		node.style.pointerEvents = 'none';
		node.style.color = 'white';
		node.style.opacity= 0.6;
		node.setAttribute = null;
	}, time * 1000);
}

function checkReward(node, count)
{
	if (node == null)
	{
		return;
	}
	
	if (count == null)
	{
		return;
	}
	
	if (voteDoneId != count + 1)
	{
		return;
	}
	
	return node.setAttribute('href', "rewards.php");
}