<?php
// Initialize the session
session_start();

// Include language file
include_once 'common.php';
require_once "config/Connect.php";
require_once "config/Configuration.php";
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
	header("location: index.php");
	exit;
}

// Get values
$username = $_SESSION["username"];

$_SESSION["charname"] = $_GET["key"];
$charId = $_SESSION["charname"];

// Done
header( "refresh:5; url=logout.php" ); 
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $lang['title'];?></title>
	<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
	<script type="text/javascript" src="js/conditions.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		body{ font: 14px sans-serif; text-align: center; }
	</style>
	
</head>
<body>
	<div class="page-header">
		<h1><b><?php echo $lang['reward_done'];?></b></h1>
		<h2><?php echo $lang['reward_thx'];?></h2>
		<br>
	</div>
		
	<?php
		$date = new DateTime();
		$newDate = $date->format('d/m/Y H:i:s');
		$newIp = $_SERVER['REMOTE_ADDR'];
		
		$sqlLogin = "INSERT INTO vote_system_login VALUES ('".$username."', '".$newIp."', '".$newDate."')";
		if ($link2->query($sqlLogin) == TRUE)
		{
			$sqlVotesUpdate = "UPDATE `vote_system_count` SET `votes`= `votes` + 1";
			$link2->query($sqlVotesUpdate);
			
			foreach ($reward_list as $itemId => $itemCount)
			{
				$sqlItem = "SELECT * FROM items WHERE owner_id = '".$charId."' AND item_id = '".$itemId."' AND loc = '".$reward_type."'";
				if ($stmt = mysqli_prepare($link2, $sqlItem))
				{
					if (mysqli_stmt_execute($stmt))
					{
						mysqli_stmt_store_result($stmt);
						if (mysqli_stmt_num_rows($stmt) == 1)
						{
							$sqlItemUpdate = "UPDATE items SET count = (count + '".$itemCount."') WHERE item_id = '".$itemId."' AND owner_id = '".$charId."'";
							$link2->query($sqlItemUpdate);
						}
						else
						{
							$objectId = 0;
							$sqlObjectId = "SELECT object_id FROM items ORDER BY object_id DESC LIMIT 1";
							$result = mysqli_query($link2, $sqlObjectId);
							if (mysqli_num_rows($result) > 0)
							{
								while($row = mysqli_fetch_assoc($result))
								{
									$objectId = $row["object_id"] + 1;
								}
							}
							
							$sqlItemAdd = "INSERT INTO items VALUES ('".$charId."', '".$objectId."', '".$itemId."', '".$itemCount."', 0, '".$reward_type."', 0, null, 0, 0, -1, -1, 0)";
							$link2->query($sqlItemAdd);
						}
					}
				}
				
				mysqli_stmt_close($stmt);
			}
		}
		else
		{
			echo $lang['reward_error'];
		}
		
		mysqli_close($link2);
	?>
</body>
</html>