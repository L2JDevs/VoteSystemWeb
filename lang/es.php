<?php

/* 
------------------
Language: Spanish
@author U3Games
------------------
*/

$lang = array();

// Properties:
$lang['title'] = 'L2JDevs Vote Web System';
$lang['vote_count'] = 'Votos totales:';

// Login panel message:
$lang['login_info'] = 'Por favor complete sus credenciales:';
$lang['login_name_empty'] = 'Por favor, ingrese un nombre de usuario!';
$lang['login_password_empty'] = 'Por favor, ingrese una contraseña!';
$lang['login_password_invalid'] = 'La clave indicada es incorrecta.';
$lang['login_name_error'] = 'La cuenta indicada no existe.';
$lang['login_error'] = 'Oops! Se ha encontrado un error. Por favor intentelo de nuevo mas tarde.';
$lang['login_create_text_1'] = 'No tienes una cuenta?';
$lang['login_create_text_2'] = 'Registrate ahora!';

// Vote panel message:
$lang['vote_title_text'] = 'Vótanos y ayúdanos a crecer!';
$lang['vote_info_text'] = 'Haga clic en cada enlace y vote por nuestro servidor. Cuando termine de votar en uno de ellos, se activará el siguiente y así sucesivamente con todos hasta que pueda reclamar la recompensa.';
$lang['vote_warning'] = 'Recuerde antes de votar para desconectar su cuenta del servidor. De lo contrario, perderá la recompensa.';

// Reward panel message:
$lang['reward_title_text'] = 'Es hora de obtener su recompensa!';
$lang['reward_info_text'] = 'A continuación le mostraremos todos los caracteres actualmente desconectados en la cuenta indicada anteriormente. Dinos a qué personaje darle la recompensa.';
$lang['reward_warning'] = 'Si no se muestran caracteres, es porque no ha cumplido con las reglas y ha perdido la recompensa. Por favor, contacte con la administración.';
$lang['reward_error'] = 'Ha habido un error con la recompensa, ¡comuníquese con la administración!';
$lang['reward_no_char_title'] = 'Has perdido tu recompensa!';
$lang['reward_no_char_text'] = 'Actualmente no hay caracteres en esta cuenta o no están fuera de línea.';
$lang['reward_already_ip'] = 'Ya habeis recibido la recompensa! ¡Su ip ha sido ya usada! ¡No intentes hacer trampa o serás penalizado!';
$lang['reward_already_time'] = 'Ya habeis recibido la recompensa! ¡Puede votar nuevamente después de 24 horas! ¡No intentes hacer trampa o seréis penalizado!';
$lang['reward_done'] = 'La recompensa se ha enviado correctamente, conecte el personaje para verificarlo.';
$lang['reward_thx'] = 'Muchas gracias por ayudarnos a crecer!';

// Single text:
$lang['login'] = 'Iniciar sesión';
$lang['username'] = 'Usuario';
$lang['password'] = 'Contraseña';
$lang['connect'] = 'Conectar';
$lang['logout'] = 'Desconectar';
$lang['claim_rewards'] = 'Reclamar recompensas';

?>