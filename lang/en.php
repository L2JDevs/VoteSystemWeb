<?php

/* 
------------------
Language: English
@author U3Games
------------------
*/

$lang = array();

// Properties:
$lang['title'] = 'L2JDevs Vote Web System';
$lang['vote_count'] = 'Total votes:';

// Login panel message:
$lang['login_info'] = 'Please fill in your credentials to login:';
$lang['login_name_empty'] = 'Please enter a username!';
$lang['login_password_empty'] = 'Please enter your password!';
$lang['login_password_invalid'] = 'The password you entered was not valid';
$lang['login_name_error'] = 'No account found with that username';
$lang['login_error'] = 'Oops! Something went wrong. Please try again later.';
$lang['login_create_text_1'] = 'Don´t have an account?';
$lang['login_create_text_2'] = 'Sign up now!';

// Vote panel message:
$lang['vote_title_text'] = 'Vote for us and help us grow!';
$lang['vote_info_text'] = 'Click on each link and vote for our server. When you finish voting in one of them, the next one will be activated and so on with everyone until you can claim the reward.';
$lang['vote_warning'] = 'Remember before voting, to have your server account disconnected. Otherwise, you will lose the reward.';

// Reward panel message:
$lang['reward_title_text'] = 'Time to get your reward!';
$lang['reward_info_text'] = 'Below we will show you all the characters currently disconnected in the account indicated above. Tell us which character to give the reward to.';
$lang['reward_warning'] = 'If no characters are shown, it is because you have not complied with the rules and you have lost the reward. Please, contact the administration.';
$lang['reward_error'] = 'There has been an error with the reward, please contact the administration!';
$lang['reward_no_char_title'] = 'You have lost your reward!';
$lang['reward_no_char_text'] = 'Currently there are no characters in this account or they are not offline.';
$lang['reward_already_ip'] = 'You have already received the reward, you ip is used! Do not try to cheat or you will be penalized!';
$lang['reward_already_time'] = 'You have already received the reward, you can vote again after 24 hours! Do not try to cheat or you will be penalized!';
$lang['reward_done'] = 'The reward has been sent correctly, please connect that character to verify it.';
$lang['reward_thx'] = 'Thank you very much for helping us to grow!';

// Single text:
$lang['login'] = 'Login';
$lang['username'] = 'Username';
$lang['password'] = 'Password';
$lang['connect'] = 'Login Account';
$lang['logout'] = 'Sign Out of Your Account';
$lang['claim_rewards'] = 'Claim Rewards';

?>