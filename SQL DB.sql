-- ----------------------------
-- Table structure for `vote_system_count`
-- ----------------------------
DROP TABLE IF EXISTS `vote_system_count`;
CREATE TABLE `vote_system_count` (
  `votes` int(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`votes`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
INSERT INTO `vote_system_count` (`votes`) VALUES (0);

-- ----------------------------
-- Table structure for `vote_system_login`
-- ----------------------------
DROP TABLE IF EXISTS `vote_system_login`;
CREATE TABLE `vote_system_login` (
  `login` VARCHAR(45) NOT NULL default '',
  `lastIP` VARCHAR(45) NULL DEFAULT NULL,
  `votedTime` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;